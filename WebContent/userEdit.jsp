<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ユーザー編集</title>

<script type="text/javascript">
	//展開・折りたたみ
	function show(inputData) {
		var objID = document.getElementById("layer_" + inputData);
		var buttonID = document.getElementById("category_" + inputData);

		if (objID.className == 'close') {
			objID.style.display = 'block';
			objID.className = 'open';
		} else {
			objID.style.display = 'none';
			objID.className = 'close';
		}
	}
</script>
</head>
<body>
    <div class="web-header">

        <div style="flex: 1 1 auto; justify-content: flex-start;">
            <a href="./" class="icon-header"></a>
        </div>
        <div style="margin-right: 10px">
            <c:out value="${loginUser.userName}" />
            さん
        </div>
        <div>
            <a href="javascript:void(0)" id="category_manu" onclick="show('manu');" class="icon-menu"></a>
        </div>
    </div>

    <div class="menu_area">
        <ul id="layer_manu" style="display: none;" class="close">
            <li><a href="contribution">ホーム</a></li>
            <c:if test="${adminFlag == true}">
                <li><a href="userManagement">ユーザー管理</a></li>
            </c:if>
            <li><a href="logout">ログアウト</a></li>
        </ul>
    </div>

    <div class="contents_area">
        <div class="main_area-s">
            <div class="title">ユーザー編集</div>
            <br />
            <%-- エラーメッセージ --%>
            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <%-- 入力フォーム --%>
            <form action="userEdit" method="post">
                <input type="hidden" name="userId" value="${user.userId}">
                <table border="1" width="600">
                    <tr>
                        <td class="field-color">
                            <label for="loginId">ログインID</label>
                            <label class="required">必須</label>
                        </td>
                        <td>
                            <input name="loginId" id="loginId" value="${user.loginId}" style="ime-mode: disabled" maxlength="20" />
                            <span class="caution">6～20文字の半角英数字</span>

                            <c:if test="${ not empty loginId_error }">
                                <div class="error">

                                    <c:out value="${loginId_error}" />
                                </div>
                            </c:if>

                        </td>
                    </tr>
                    <tr>
                        <td class="field-color">
                            <label for="userName">氏名</label>
                            <label class="required">必須</label>
                        </td>
                        <td>
                            <input name="userName" id="userName" value="${user.userName}" maxlength="10" />
                            <span class="caution">10文字以内</span>

                            <c:if test="${ not empty userName_error }">
                                <div class="error">

                                    <c:out value="${userName_error}" />
                                </div>
                            </c:if>

                        </td>
                    </tr>

                    <tr>
                        <td class="field-color">
                            <label for="newPassword" style="ime-mode: disabled">新しいパスワード</label>
                        </td>
                        <td>
                            <input name="newPassword" type="password" id="newPassword" maxlength="20" />
                            <span class="caution">6～20文字の半角文字(記号含む)</span>

                            <c:if test="${ not empty newPassword_error }">
                                <div class="error">

                                    <c:out value="${newPassword_error}" />
                                </div>
                            </c:if>

                        </td>
                    </tr>

                    <tr>
                        <td class="field-color">
                            <label for="newPassword2" style="ime-mode: disabled">新しいパスワード(確認用)</label>
                        </td>
                        <td>
                            <input name="newPassword2" type="password" id="newPassword2" maxlength="20" />
                            <span class="caution">6～20文字の半角文字(記号含む)</span>

                            <c:if test="${ not empty newPassword2_error }">
                                <div class="error">

                                    <c:out value="${newPassword2_error}" />
                                </div>
                            </c:if>

                        </td>
                    </tr>

                    <tr>
                        <td class="field-color">
                            <label for="branch">支店</label>
                            <label class="required">必須</label>
                        </td>

                        <td>
                            <c:if test="${loginUser.userId != user.userId}">
                                <select name="branch">
                                    <option value=""></option>
                                    <c:forEach items="${branchMap}" var="branch">

                                        <c:if test="${ branch.key == user.branchId}">
                                            <option value="${branch.key}" selected>${branch.value}</option>
                                        </c:if>

                                        <c:if test="${ branch.key != user.branchId or empty user.branchId}">
                                            <option value="${branch.key}">${branch.value}</option>
                                        </c:if>
                                    </c:forEach>
                                </select>
                            </c:if>
                            <c:if test="${loginUser.userId == user.userId}">
                                    ${branchMap[loginUser.branchId]}
                                    <input type="hidden" name="branch" value="${loginUser.branchId}">
                                <a class="icon-lock inline"></a>
                            </c:if>

                            <c:if test="${ not empty branch_error }">
                                <div class="error">

                                    <c:out value="${branch_error}" />
                                </div>
                            </c:if>

                        </td>
                    </tr>

                    <tr>
                        <td class="field-color">
                            <label for="position">部署・役職</label>
                            <label class="required">必須</label>
                        </td>
                        <td>
                            <c:if test="${loginUser.userId != user.userId}">
                                <select name="position">
                                    <option value=""></option>
                                    <c:forEach items="${positionMap}" var="position">

                                        <c:if test="${ position.key == user.positionId}">
                                            <option value="${position.key}" selected>${position.value}</option>
                                        </c:if>

                                        <c:if test="${ position.key != user.positionId or empty user.positionId}">
                                            <option value="${position.key}">${position.value}</option>
                                        </c:if>

                                    </c:forEach>
                                </select>
                            </c:if>

                            <c:if test="${loginUser.userId == user.userId}">
                                    ${positionMap[loginUser.positionId]}
                                    <input type="hidden" name="position" value="${loginUser.positionId}">
                                <a class="icon-lock inline"></a>
                            </c:if>

                            <c:if test="${ not empty position_error }">
                                <div class="error">
                                    <c:out value="${position_error}" />
                                </div>
                            </c:if>

                        </td>
                    </tr>

                </table>
                <div class="input-line">
                    <input type="button" value="キャンセル" onclick="location.href='userManagement'" class="send-btn  soft-gloss" />
                    <input type="submit" value="OK" class="send-btn  soft-gloss" />
                </div>
            </form>
        </div>
        <div class="copyright">Copyright(c) 2018 UnoMari</div>
    </div>
</body>
</html>