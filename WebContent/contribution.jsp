<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>新規投稿</title>

<script type="text/javascript">
	//展開・折りたたみ
	function show(inputData) {
		var objID = document.getElementById("layer_" + inputData);
		var buttonID = document.getElementById("category_" + inputData);

		if (objID.className == 'close') {
			objID.style.display = 'block';
			objID.className = 'open';
		} else {
			objID.style.display = 'none';
			objID.className = 'close';
		}
	}
</script>
</head>
<body>
    <div class="web-header">

        <div style="flex: 1 1 auto; justify-content: flex-start;">
            <a href="./" class="icon-header"></a>
        </div>
        <div style="margin-right: 10px">
            <c:out value="${loginUser.userName}" />
            さん
        </div>
        <div>
            <a href="javascript:void(0)" id="category_manu" onclick="show('manu');" class="icon-menu"></a>
        </div>
    </div>

    <div class="menu_area">
        <ul id="layer_manu" style="display: none;" class="close">
            <li><a href="contribution">ホーム</a></li>
            <c:if test="${adminFlag == true}">
                <li><a href="userManagement">ユーザー管理</a></li>
            </c:if>
            <li><a href="logout">ログアウト</a></li>
        </ul>
    </div>

    <div class="contents_area">
        <div class="main_area-s">
            <div class="title">新規投稿</div>
            <div>
                <div class="post-from">

                    <%-- 入力フォーム --%>

                    <form action="contribution" method="post">
                        <div class="post-input-area">

                            <div class="input-element">
                                <label>件名</label>
                                <label class="required">必須</label>
                            </div>

                            <c:if test="${ not empty subject_error }">
                                <div class="error">
                                     <c:out value="${subject_error}" />
                                </div>
                            </c:if>

                            <input name="subject" id="subject" value="${post.subject}" maxlength="30" class="subject-text" />

                            <div class="input-element">
                                <label>カテゴリー</label>
                                <label class="required">必須</label>
                            </div>

                            <c:if test="${ not empty category_error }">
                                <div class="error">
                                     <c:out value="${category_error}" />
                                </div>
                            </c:if>

                            <input name="category" id="category" value="${post.category}" maxlength="10" />

                            <div class="input-element">
                                <label>本文</label>
                                <label class="required">必須</label>
                            </div>

                            <c:if test="${ not empty postText_error }">
                                <div class="error">
                                    <c:out value="${postText_error}" />
                                </div>
                            </c:if>

                            <textarea name="postText" rows="10" class="textbox">${post.postText}</textarea>
                            <br />
                            <div class="post-input-line">
                                <input type="button" value="キャンセル" onclick="location.href='./'" class="send-btn  soft-gloss" />
                                <input type="submit" value="送信" class="send-btn  soft-gloss" />
                            </div>
                        </div>
                        <br />
                    </form>
                </div>
            </div>
        </div>
        <div class="copyright">Copyright(c) 2018 UnoMari</div>
    </div>
</body>
</html>