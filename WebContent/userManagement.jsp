<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script src="js/jquery-latest.js" type="text/javascript"></script>
<script src="js/jquery.tablesorter.min.js" type="text/javascript"></script>

<script type="text/javascript">
	function dialog(message, alert) {

		var result = window.confirm(message);

		// 「OK」押下時
		if (result) {
			window.alert(alert);
			return true;

			// 「キャンセル」押下時
		} else {
			window.alert('キャンセルされました');
			return false;
		}

	}

	//展開・折りたたみ
	function show(inputData) {
		var objID = document.getElementById("layer_" + inputData);
		var buttonID = document.getElementById("category_" + inputData);

		if (objID.className == 'close') {
			objID.style.display = 'block';
			objID.className = 'open';
		} else {
			objID.style.display = 'none';
			objID.className = 'close';
		}
	}

	$(document).ready(function() {

		$("#sortTable").tablesorter({

			headers : {
				5 : {
					sorter : false
				},
				4 : {
					sorter : false
				}
			}
		});
	});
</script>

<link href="./css/style.css" rel="stylesheet" type="text/css">
<link href="./css/table.css" rel="stylesheet" type="text/css">
<title>ユーザー管理</title>
</head>
<body>

    <div class="web-header">

        <div style="flex: 1 1 auto; justify-content: flex-start;">
            <a href="./" class="icon-header"></a>
        </div>
        <div style="margin-right: 10px">
            <c:out value="${loginUser.userName}" />
            さん
        </div>
        <div>
            <a href="javascript:void(0)" id="category_manu" onclick="show('manu');" class="icon-menu"></a>
        </div>
    </div>

    <div class="menu_area">
        <ul id="layer_manu" style="display: none;" class="close">
            <li><a href="contribution">ホーム</a></li>
            <li><a href="userManagement">ユーザー管理</a></li>
            <li><a href="logout">ログアウト</a></li>
        </ul>
    </div>

    <div class="contents_area">
        <%-- エラーメッセージ --%>
        <c:if test="${ not empty errorMessages }">
            <div class="error-big">

                <span class="icon-error"></span>
                <c:out value="${errorMessages}" />
            </div>

            <c:remove var="errorMessages" scope="session" />
        </c:if>


        <div class="userRegist">
            <a href="userRegistration" class="icon-user_add">ユーザー登録</a>
        </div>

        <div class="main_area">
            <div class="title">ユーザー管理</div>

            <div class="userSearch">
                <form action="userManagement" method="get">

                    <label for="userName">名前</label>
                    <input name="userName" id="userName" value="${userName}" class="userSearchWord" />

                    <label for="loginId">ログインID</label>
                    <input name="loginId" id="loginId" value="${loginId}" class="userSearchWord" />

                    <label for="branch">支店</label>

                    <c:if test="${loginUser.userId != user.userId}">
                        <select name="branch">
                            <option value=""></option>
                            <c:forEach items="${branchMap}" var="branch">

                                <c:if test="${ branch.key == branchId}">
                                    <option value="${branch.key}" selected>${branch.value}</option>
                                </c:if>

                                <c:if test="${ branch.key != branchId or empty branchId}">
                                    <option value="${branch.key}">${branch.value}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </c:if>

                    <label for="position">役職</label>

                    <c:if test="${loginUser.userId != userId}">
                        <select name="position">
                            <option value=""></option>
                            <c:forEach items="${positionMap}" var="position">

                                <c:if test="${ position.key == positionId}">
                                    <option value="${position.key}" selected>${position.value}</option>
                                </c:if>

                                <c:if test="${ position.key != positionId or empty positionId}">
                                    <option value="${position.key}">${position.value}</option>
                                </c:if>

                            </c:forEach>
                        </select>
                    </c:if>

                    <input type="submit" value="検索" class="search-btn soft-gloss">
                    <input type="button" value="リセット" onClick="location.href='userManagement';" class="search-btn soft-gloss">
                </form>

            </div>

            <table border="1" width="95%" id="sortTable" class="tablesorter">
                <thead>
                    <tr>
                        <th>名前</th>
                        <th>ログインID</th>
                        <th>支店</th>
                        <th>役職</th>
                        <th>利用停止</th>
                        <th>編集</th>

                    </tr>
                </thead>

                <tbody>
                    <c:forEach items="${users}" var="user">
                        <tr ${user.useFlag == 1 ? 'bgcolor="#ffffff"' : 'bgcolor="#808080"'}>
                            <td>
                                <c:out value="${user.userName}" />
                            </td>
                            <td>
                                <c:out value="${user.loginId}" />
                            </td>
                            <td>
                                <c:out value="${user.branchName}" />
                            </td>
                            <td>
                                <c:out value="${user.positionName}" />
                            </td>

                            <td class="table-center">
                                <%-- 停止 --%>
                                <c:if test="${user.useFlag == 1}">
                                    <form action="userManagement" method="post">
                                        <input type="hidden" name="userId" value="${user.userId}">
                                        <input type="hidden" name=useFlag value="0">

                                        <input type="submit" value="停止" onClick="return dialog('停止させますか?','停止させました')"
                                            ${loginUser.userId == user.userId ? 'disabled="disabled" class="stop-btn-disable soft-gloss"':'class="stop-btn soft-gloss"'} />

                                    </form>
                                </c:if>
                                <%-- 復活 --%>
                                <c:if test="${user.useFlag == 0}">
                                    <form action="userManagement" method="post">
                                        <input type="hidden" name="userId" value="${user.userId}">
                                        <input type="hidden" name=useFlag value="1">
                                        <input type="submit" value="復活" onClick="return dialog('復活させますか?','復活させました')"
                                            ${loginUser.userId == user.userId ? 'disabled="disabled" class="reopen-btn-disable soft-gloss"':'class="reopen-btn soft-gloss"'} />
                                    </form>
                                </c:if>
                            </td>

                            <td class="table-center">
                                <form action="userEdit" method="get">
                                    <input type="hidden" name="userId" value="${user.userId}">
                                    <input type="submit" value="編集" class="normal-btn soft-gloss" />
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>

            </table>
            <br />
        </div>
    </div>

    <div class="copyright">Copyright(c) 2018 UnoMari</div>
</body>
</html>