<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script src="js/jquery-3.3.1.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/jquery-ui.min.js"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-ja.min.js"></script>
<script>
	$(function() {
		$("#datepicker1").datepicker();

		$("#datepicker2").datepicker();
	});
</script>

<script type="text/javascript">
	//投稿・コメント削除ダイアログ
	function dialog() {

		// 「OK」押下時
		if (window.confirm('削除しますか？')) {
			// 削除
			return true;
		}

		// 「キャンセル」押下時
		else {

			window.alert('キャンセルしました');
			return false;

		}
	}

	//展開・折りたたみ
	function show(inputData) {
		var objID = document.getElementById("layer_" + inputData);
		var buttonID = document.getElementById("category_" + inputData);

		if (objID.className == 'close') {
			objID.style.display = 'block';
			objID.className = 'open';
		} else {
			objID.style.display = 'none';
			objID.className = 'close';
		}
	}
</script>

<script type="text/javascript">
	$(document).ready(function() {
		$(".sampleTables").tablesorter();
	});
</script>

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ホーム</title>
</head>
<body>
    <div class="web-header">

        <div style="flex: 1 1 auto; justify-content: flex-start;">
            <a href="./" class="icon-header"></a>
        </div>
        <div style="margin-right: 20px">
            <c:out value="${loginUser.userName}" />
            さん
        </div>

        <a href="contribution" class="icon-contribution"></a>

        <a href="javascript:void(0)" id="category_search" onclick="show('search');" class="icon-search"></a>

        <a href="javascript:void(0)" id="category_manu" onclick="show('manu');" class="icon-menu"></a>
    </div>

    <div class="menu_area">
        <ul id="layer_manu" style="display: none;" class="close">
            <li><a href="contribution">ホーム</a></li>
            <c:if test="${adminFlag == true}">
                <li><a href="userManagement">ユーザー管理</a></li>
            </c:if>
            <li><a href="logout">ログアウト</a></li>
        </ul>
    </div>

    <div class="search-bar">

        <div id="layer_search" style="display: none" class="close">

            <div class="search-element">
                <form action="index.jsp" method="get">
                    <label for="categoryWord">カテゴリー</label>
                    <input name="categoryWord" id="categoryWord" value="${categoryWord}" />
                    期間検索
                    <input type="text" id="datepicker1" name="startDate" value="${startDate}" readonly="readonly" class="date" />
                    ～
                    <input type="text" id="datepicker2" name="endDate" value="${endDate}" readonly="readonly" class="date" />
                    <input type="submit" value="検索" class="search-btn soft-gloss">
                    <input type="button" value="リセット" onClick="location.href='./';" class="search-btn soft-gloss">
                </form>
            </div>

        </div>

    </div>

    <div class="contents_area">

        <%-- エラーメッセージ --%>

        <c:out value="${postError}" />

        <!-- 投稿エリア  -->
        <c:forEach items="${posts}" var="post">
            <div class="post-area">
                <div class="post-title">
                    <c:out value="${post.subject}" />
                </div>
                <div class="post-info">
                    <p class="post-contributor">
                        投稿者：
                        <c:out value="${post.userName}" />
                    </p>
                    <p class="post-category">
                        カテゴリー：
                        <c:out value="${post.category}" />
                    </p>
                </div>
                <p>
                    <c:forEach var="postText" items="${fn:split(post.postText,'
')}">
                        <c:out value="${postText}" />
                        <br>
                    </c:forEach>
                <div class="post-comment">
                    <a href="javascript:void(0)" id="category_${post.postId}" onclick="show('${post.postId}');">コメント（${fn:length(post.comments)}）</a>
                </div>

                <div class="post-date">
                    <c:out value="${post.createdDate}" />
                </div>
                <div class="post-delete">

                    <!-- 投稿削除ボタン  -->
                    <c:if test="${loginUser.userId == post.userId}">
                        <form action="postDelete" method="post">
                            <input type="hidden" name="postId" value="${post.postId}">
                            <input type="submit" value="削除" onClick="return dialog()" class="delete-btn soft-gloss">
                        </form>
                    </c:if>
                </div>
                <br />

                <!-- コメントエリア  -->

                <div class="comment-area">

                    <div id="layer_${post.postId}" ${openflag==true ? 'class="open"':'style="display: none" class="close"'}>

                        <div class="comment-title">コメント（500文字以内）</div>
                        <c:forEach items="${post.comments}" var="comments" varStatus="status">
                            <div class="comment">

                                <c:out value="${status.count}" />
                                .
                                <c:out value="${comments.userName}" />
                                <br />

                                <div class="comment-text">
                                    <c:forEach var="commentText" items="${fn:split(comments.commentText,'
')}">
                                        <c:out value="${commentText}" />
                                        <br>
                                    </c:forEach>
                                </div>

                                <p class="comment-date">
                                    <c:out value="${comments.updatedDate}" />
                                </p>
                                <!-- コメント削除ボタン  -->
                                <c:if test="${loginUser.userId == comments.userId}">

                                    <form action="commentDelete" method="post">
                                        <input type="hidden" name="commentId" value="${comments.commentId}">
                                        <p class="comment-delete">
                                            <input type="submit" value="削除" onClick="return dialog()" class="delete-btn soft-gloss">
                                        </p>
                                    </form>

                                </c:if>
                            </div>
                            <hr>
                        </c:forEach>

                        <div class="comment">
                            <c:if test="${ not empty comment_error }">
                                <div class="error-big">

                                    <span class="icon-error"></span>
                                    <c:out value="${comment_error}" />
                                </div>

                                <c:remove var="comment_error" scope="session" />
                            </c:if>

                            <form action="CommentRegistration" method="post">

                                <div class="comment-form">
                                    <input type="hidden" name="postId" value="${post.postId}">
                                    <input type="hidden" name="userId" value="${loginUser.userId}">
                                    <textarea name="comment" class="comment-box" rows="5"></textarea>
                                    <input type="submit" value="送信" class="send-btn soft-gloss comment-button">
                                </div>

                            </form>
                            <c:remove var="comment" scope="session" />
                            <c:remove var="openflag" scope="session" />
                        </div>
                    </div>
                </div>
            </div>
        </c:forEach>

        <div class="copyright">Copyright(c) 2018 UnoMari</div>
    </div>
    <c:remove var="commonFlag" scope="session" />
</body>
</html>