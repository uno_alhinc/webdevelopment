<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="./css/style.css" rel="stylesheet" type="text/css">
<title>ログイン</title>
</head>
<body>

    <div class="web-header">
        <div style="flex: 1 1 auto; justify-content: flex-start;">
            <a href="login" class="icon-header"></a>
        </div>
    </div>

    <div class="contents_area">
        <c:if test="${ not empty errorMessages }">
            <div class="errorMessages">
                <ul>
                    <c:forEach items="${errorMessages}" var="message">
                        <li><c:out value="${message}" />
                    </c:forEach>
                </ul>
            </div>
            <c:remove var="errorMessages" scope="session" />
        </c:if>
        <div class="main_area-login">
            <div class="title">ログイン</div>
            <div class="login-area">

                <c:if test="${ not empty errorMessage }">
                    <div class="error-big">

                        <span class="icon-error"></span>
                        <c:out value="${errorMessage}" />
                    </div>

                    <c:remove var="errorMessage" scope="session" />
                </c:if>

                <form action="login" method="post">
                    <div>
                        <label for="loginid">ログインID</label>
                    </div>

                    <div class="login-line">
                        <input name="loginid" id="loginid" value="${loginid}" style="ime-mode: disabled" maxlength="20" class="login-text"
                            placeholder="loginId" />
                    </div>

                    <div>
                        <label for="password">パスワード</label>
                    </div>

                    <div class="login-line">
                        <input name="password" type="password" id="password" style="ime-mode: disabled" maxlength="20" class="login-text"
                            placeholder="password" />
                    </div>
                    <div class="login-botton">
                        <input type="submit" value="ログイン" class="send-btn  soft-gloss" />
                    </div>
                </form>
            </div>
        </div>
        <div class="copyright">Copyright(c) 2018 UnoMari</div>
    </div>
</body>
</html>