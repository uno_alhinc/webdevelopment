package uno_mari.controller;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import uno_mari.beans.Post;
import uno_mari.service.HomeService;

@WebServlet("/index.jsp")
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //カテゴリー
        String categoryWord = "";

        //現在時刻
        Calendar calendar = Calendar.getInstance();
        Timestamp endDate = new Timestamp(calendar.getTimeInMillis());

        //一年前
        calendar.add(Calendar.YEAR, -1);
        Timestamp startDate = new Timestamp(calendar.getTimeInMillis());

        //開始日付取得
        if (request.getParameter("startDate") != null && !request.getParameter("startDate").equals("")) {
            String strStartDate = request.getParameter("startDate");

            try {
                startDate = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(strStartDate).getTime());
                request.setAttribute("startDate", strStartDate);
            } catch (ParseException e) {

            }
        }

        //終了日付取得
        if (request.getParameter("endDate") != null && !request.getParameter("endDate").equals("")) {
            String strEndDate = request.getParameter("endDate");

            try {
                endDate = new Timestamp(new SimpleDateFormat("yyyy/MM/dd").parse(strEndDate).getTime());
                request.setAttribute("endDate", strEndDate);
            } catch (ParseException e) {

            }
        }

        //カテゴリー取得
        if (request.getParameter("categoryWord") != null) {
            categoryWord = request.getParameter("categoryWord");
            request.setAttribute("categoryWord", categoryWord);
        }

        //投稿リスト取得
        List<Post> posts = new HomeService().getPosts(categoryWord, startDate, endDate);

        if (posts.size() == 0) {
            String postError = "・記事が存在しません。";
            request.setAttribute("postError", postError);
        }

        request.setAttribute("posts", posts);

        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

}
