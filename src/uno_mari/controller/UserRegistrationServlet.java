package uno_mari.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import uno_mari.beans.User;
import uno_mari.service.UserRegistrationService;
import uno_mari.service.UserService;

@WebServlet("/userRegistration")
public class UserRegistrationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        HashMap<Integer, String> branchMap = new UserRegistrationService().getBranches();
        HashMap<Integer, String> positionMap = new UserRegistrationService().getPositions();

        //セッション
        HttpSession session = request.getSession();

        session.setAttribute("branchMap", branchMap);
        session.setAttribute("positionMap", positionMap);
        request.getRequestDispatcher("/userRegistration.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Beans
        User user = new User();

        //取得した値を設定する
        user.setLoginId(request.getParameter("loginId"));
        user.setUserName(request.getParameter("userName"));
        user.setPassword(request.getParameter("password"));
        user.setPassword2(request.getParameter("password2"));

        //バリデーション
        if (isValid(request, user) == true) {
            new UserService().register(user);

            //リダイレクト
            response.sendRedirect("userManagement");

        } else {

            //JSPに入力したデータを引き渡す
            request.setAttribute("user", user);
            request.getRequestDispatcher("userRegistration.jsp").forward(request, response);
        }

    }

    private boolean isValid(HttpServletRequest request, User user) {

        boolean isValid = true;

        String loginId = request.getParameter("loginId");
        String userName = request.getParameter("userName");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String branch = request.getParameter("branch");
        String position = request.getParameter("position");

        //ログインIDが未入力ならエラー
        if (StringUtils.isBlank(loginId) == true) {
            request.setAttribute("loginId_error", "ログインIDを入力してください。");
            isValid = false;
        }

        //ログインIDが、6文字以上20文字以内の半角英数字以外の場合はエラー
        else if (!loginId.matches("^[0-9a-zA-Z]{6,20}$")) {
            request.setAttribute("loginId_error", "ログインIDは、6～20文字の半角英数字で入力してください。");
            isValid = false;

        }

        if (!new UserService().userCheck(loginId)) {
            request.setAttribute("loginId_error", "既に登録されているログインIDです。");
            isValid = false;

        }

        //氏名が未入力ならエラー
        if (StringUtils.isBlank(userName) == true) {
            request.setAttribute("userName_error", "氏名を入力してください。");
            isValid = false;

        }
        //氏名が、10文字以内じゃない場合はエラー
        else if (userName.length() > 10) {
            request.setAttribute("userName_error", "氏名は10文字以内で入力してください。");
            isValid = false;

        }

        //パスワードが未入力ならエラー
        if (StringUtils.isBlank(password) == true) {
            request.setAttribute("password_error", "パスワードを入力してください。");
            isValid = false;

        }
        //パスワードが、6文字以上20文字以内の半角文字(記号含む)じゃない場合はエラー
        else if (!password.matches("^\\w{6,20}")) {
            request.setAttribute("password_error", "パスワードは、6～20文字の半角文字(記号含む)で入力してください。");
            isValid = false;

        }

        //パスワード(確認)が未入力ならエラー
        if (StringUtils.isBlank(password2) == true) {
            request.setAttribute("password2_error", "パスワード(確認)を入力してください。");
            isValid = false;

        }
        //パスワードが、6文字以上20文字以内の半角文字(記号含む)じゃない場合はエラー
        else if (!password2.matches("^\\w{6,20}")) {
            request.setAttribute("password2_error", "パスワード(確認)は、6～20文字の半角文字(記号含む)で入力してください。");
            isValid = false;

        }

        //パスワードとパスワード(確認)が一致しない場合はエラー
        if (!StringUtils.equals(password, password2) == true && !StringUtils.isBlank(password) == true && !StringUtils.isBlank(password2)) {
            request.setAttribute("password_error", "パスワードが一致しません。");
            isValid = false;

        }

        //支店が未選択ならエラー
        if (StringUtils.isBlank(branch) == true) {
            request.setAttribute("branch_error", "支店を選択してください。");
            isValid = false;

        } else {
            user.setBranchId(Integer.valueOf(request.getParameter("branch")));
        }

        //部署・役職が未入力ならエラー
        if (StringUtils.isBlank(position) == true) {
            request.setAttribute("position_error", "部署・役職を選択してください");
            isValid = false;

        } else {
            user.setPositionId(Integer.valueOf(request.getParameter("position")));
        }

        return isValid;

    }

}
