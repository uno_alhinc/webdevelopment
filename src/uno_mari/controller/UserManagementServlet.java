package uno_mari.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import uno_mari.beans.User;
import uno_mari.service.UserManagementService;
import uno_mari.service.UserRegistrationService;

@WebServlet("/userManagement")
public class UserManagementServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //セッション
        HttpSession session = request.getSession();

        //ユーザー名
        String userName = "";

        //ログインID
        String loginId = "";

        //支店
        int branch = 0;

        //役職
        int position = 0;

        //ユーザー名取得
        if (request.getParameter("userName") != null) {
            userName = request.getParameter("userName");
            request.setAttribute("userName", userName);
        }

        //ログインID取得
        if (request.getParameter("loginId") != null) {
            loginId = request.getParameter("loginId");
            request.setAttribute("loginId", loginId);
        }

        //支店コード取得
        if (request.getParameter("branch") != null) {

            try{
                branch = Integer.parseInt(request.getParameter("branch"));
                request.setAttribute("branchId", branch);
            }catch(NumberFormatException e){
            }
        }

        //役職コード取得
        if (request.getParameter("position") != null) {

            try{
                position = Integer.parseInt(request.getParameter("position"));
                request.setAttribute("positionId", position);
            }catch(NumberFormatException e){
            }
        }

        //ユーザーリスト取得
        List<User> users = new UserManagementService().getUsers(userName, loginId, branch, position);

        //支店リスト、支店・役職リスト
        HashMap<Integer, String> branchMap = new UserRegistrationService().getBranches();
        HashMap<Integer, String> positionMap = new UserRegistrationService().getPositions();

        session.setAttribute("branchMap", branchMap);
        session.setAttribute("positionMap", positionMap);

        request.setAttribute("users", users);
        request.getRequestDispatcher("/userManagement.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //取得した値を設定する
        int userId = Integer.parseInt(request.getParameter("userId"));
        int useFlag = Integer.parseInt(request.getParameter("useFlag"));

        new UserManagementService().useFlagUpdate(userId, useFlag);

        //リダイレクト
        response.sendRedirect("userManagement");
    }

}
