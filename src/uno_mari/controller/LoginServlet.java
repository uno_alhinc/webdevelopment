package uno_mari.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import uno_mari.beans.User;
import uno_mari.service.LoginService;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String loginid = request.getParameter("loginid");
        String password = request.getParameter("password");

        LoginService loginService = new LoginService();
        User user = loginService.login(loginid, password);

        HttpSession session = request.getSession();

        if (user != null) {

            boolean adminFlag = false;

            if (user.getBranchId() == 1 && user.getPositionId() == 4) {
                adminFlag = true;
            }

            session.setAttribute("adminFlag", adminFlag);
            session.setAttribute("loginUser", user);
            response.sendRedirect("./");
        } else {

            session.setAttribute("errorMessage", "ログインに失敗しました。");
            request.setAttribute("loginid", loginid);
            request.getRequestDispatcher("login.jsp").forward(request, response);
        }
    }

}
