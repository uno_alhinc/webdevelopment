package uno_mari.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import uno_mari.beans.Post;
import uno_mari.beans.User;
import uno_mari.service.ContributionService;

@WebServlet("/contribution")
public class ContributionServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/contribution.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //セッション
        HttpSession session = request.getSession();

        //Beans
        Post post = new Post();

        //取得した値を設定する
        post.setSubject(request.getParameter("subject"));
        post.setCategory(request.getParameter("category"));
        post.setPostText(request.getParameter("postText"));


        if (isValid(request) == true) {
            User user = (User) session.getAttribute("loginUser");
            post.setUserId(user.getUserId());
            new ContributionService().register(post);

            //リダイレクト
            response.sendRedirect("./");

        } else {

            //JSPに入力したデータを引き渡す
            request.setAttribute("post", post);
            request.getRequestDispatcher("/contribution.jsp").forward(request, response);
        }

    }

    private boolean isValid(HttpServletRequest request) {

        boolean isValid = true;

        String subject = request.getParameter("subject");
        String category = request.getParameter("category");
        String postText = request.getParameter("postText");

        //件名が未入力ならエラー
        if (StringUtils.isBlank(subject) == true) {

            request.setAttribute("subject_error", "件名を入力してください。");
            isValid = false;
        }
        //氏名が30文字以内じゃない場合はエラー
        else if (subject.length() > 30) {

            request.setAttribute("subject_error", "件名は30文字以内で入力してください。");
            isValid = false;
        }

        //カテゴリーが未入力ならエラー
        if (StringUtils.isBlank(category) == true) {

            request.setAttribute("category_error", "カテゴリーを入力してください。");
            isValid = false;
        }
        //カテゴリーが30文字以内じゃない場合はエラー
        else if (category.length() > 30) {

            request.setAttribute("category_error", "件名は30文字以内で入力してください。");
            isValid = false;
        }

        //本文が未入力ならエラー
        if (StringUtils.isBlank(postText) == true) {
            request.setAttribute("postText_error", "本文を入力してください。");
            isValid = false;

        }
        //本文が1000文字以内じゃない場合はエラー
        else if (category.length() > 30) {
            request.setAttribute("postText_error", "本文は1000文字以内で入力してください。");
            isValid = false;

        }

        return isValid;
    }

}
