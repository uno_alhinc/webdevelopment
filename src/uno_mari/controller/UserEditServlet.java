package uno_mari.controller;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import uno_mari.beans.User;
import uno_mari.service.UserEditService;
import uno_mari.service.UserRegistrationService;
import uno_mari.service.UserService;

@WebServlet("/userEdit")
public class UserEditServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //ユーザー情報
        User user = null;

        //セッション
        HttpSession session = request.getSession();

        //支店リスト、支店・役職リスト
        HashMap<Integer, String> branchMap = new UserRegistrationService().getBranches();
        HashMap<Integer, String> positionMap = new UserRegistrationService().getPositions();

        session.setAttribute("branchMap", branchMap);
        session.setAttribute("positionMap", positionMap);

        //不正フラグ
        boolean illegalFlag = false;

        try {
            user = new UserEditService().getUser(Integer.parseInt(request.getParameter("userId")));

            //不正パラメータチェック
            if (user == null) {
                illegalFlag = true;
            }

        } catch (NumberFormatException e) {
            illegalFlag = true;
        }

        if (illegalFlag) {

            //リダイレクト
            session.setAttribute("errorMessages", "不正なパラメータです。");
            response.sendRedirect("userManagement");

        } else {
            request.setAttribute("user", user);
            request.getRequestDispatcher("/userEdit.jsp").forward(request, response);

        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //Beans
        User user = new User();

        //取得した値を設定する
        user.setUserId(Integer.parseInt(request.getParameter("userId")));
        user.setLoginId(request.getParameter("loginId"));
        user.setUserName(request.getParameter("userName"));

        //バリデーション
        if (isValid(request, user) == true) {

            user.setPassword(request.getParameter("newPassword"));
            new UserEditService().register(user);

            //リダイレクト
            response.sendRedirect("userManagement");

        } else {

            //JSPに入力したデータを引き渡す
            request.setAttribute("user", user);
            request.getRequestDispatcher("userEdit.jsp").forward(request, response);
        }

    }

    private boolean isValid(HttpServletRequest request, User user) {

        boolean isValid = true;

        int userId = Integer.parseInt(request.getParameter("userId"));
        String loginId = request.getParameter("loginId");
        String userName = request.getParameter("userName");
        String newPassword = request.getParameter("newPassword");
        String newPassword2 = request.getParameter("newPassword2");
        String branch = request.getParameter("branch");
        String position = request.getParameter("position");

        //ログインIDが未入力ならエラー
        if (StringUtils.isBlank(loginId) == true) {
            request.setAttribute("loginId_error", "ログインIDを入力してください。");
            isValid = false;

        }

        //ログインIDが、6文字以上20文字以内の半角英数字以外の場合はエラー
        else if (!loginId.matches("^[0-9a-zA-Z]{6,20}$")) {
            request.setAttribute("loginId_error", "ログインIDは、6～20文字の半角英数字で入力してください。");
            isValid = false;

        }

        if (!new UserService().userCheck(loginId, userId)) {
            request.setAttribute("loginId_error", "既に登録されているログインIDです。");
            isValid = false;
        }

        //氏名が未入力ならエラー
        if (StringUtils.isBlank(userName)) {
            request.setAttribute("userName_error", "氏名を入力してください。");
            isValid = false;

        }
        //氏名が、10文字以内じゃない場合はエラー
        else if (userName.length() > 10) {
            request.setAttribute("userName_error", "氏名は10文字以内で入力してください。");
            isValid = false;
        }

        //新しいパスワードが、6文字以上20文字以内の半角文字(記号含む)じゃない場合はエラー
        if (!newPassword.matches("[ -~]{6,20}") && !StringUtils.isBlank(newPassword)) {
            request.setAttribute("newPassword_error", "新しいパスワードは、6～20文字の半角文字(記号含む)で入力してください。");
            isValid = false;
        }

        //新しいパスワード(確認用)が、6文字以上20文字以内の半角文字(記号含む)じゃない場合はエラー
        if (!newPassword2.matches("[ -~]{6,20}") && !StringUtils.isBlank(newPassword2)) {
            request.setAttribute("newPassword2_error", "新しいパスワード(確認)は、6～20文字の半角文字(記号含む)で入力してください。");
            isValid = false;
        }

        //新しいパスワードと新しいパスワード(確認)が一致しない場合はエラー
        if (!StringUtils.equals(newPassword, newPassword2) && !StringUtils.isBlank(newPassword) && !StringUtils.isBlank(newPassword2)) {
            request.setAttribute("newPassword_error", "パスワードが一致しません。");
            isValid = false;
        }

        //支店が未選択ならエラー
        if (StringUtils.isBlank(branch)) {
            request.setAttribute("branch_error", "支店を選択してください。");
            isValid = false;
        } else {
            user.setBranchId(Integer.valueOf(request.getParameter("branch")));
        }

        //部署・役職が未入力ならエラー
        if (StringUtils.isBlank(position)) {
            request.setAttribute("position_error", "部署・役職を選択してください");
            isValid = false;
        } else {
            user.setPositionId(Integer.valueOf(request.getParameter("position")));
        }

        return isValid;

    }

}
