package uno_mari.dao;

import static uno_mari.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import uno_mari.beans.Comment;
import uno_mari.exception.SQLRuntimeException;

public class CommentDao {

    public List<Comment> getComments(Connection connection, int postId) {
        PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT *");
            sql.append(" FROM");
            sql.append(" comments INNER JOIN users");
            sql.append(" ON");
            sql.append(" comments.user_id = users.user_id");
            sql.append(" WHERE");
            sql.append(" comments.delete_flag = '0'");
            sql.append(" AND");
            sql.append(" comments.post_id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, postId);
            ResultSet rs = ps.executeQuery();

            List<Comment> commentList = new ArrayList<Comment>();
            //コメントリスト取得
            commentList = toPostList(rs);
            return commentList;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //コメントリスト作成
    private List<Comment> toPostList(ResultSet rs) throws SQLException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd  HH:mm:ss");

        List<Comment> ret = new ArrayList<Comment>();
        try {
            //DBから取得したデータをリストに代入する
            while (rs.next()) {

                int commentId = rs.getInt("comment_id");
                int postId = rs.getInt("post_id");
                String commentText = rs.getString("comment_text");
                int deleteFlag = rs.getInt("post_id");
                int userId = rs.getInt("user_id");
                String userName = rs.getString("user_name");
                String createdDate = sdf.format(rs.getTimestamp("created_date"));
                String updatedDate = sdf.format(rs.getTimestamp("updated_date"));

                Comment comment = new Comment();

                comment.setCommentId(commentId);
                comment.setPostId(postId);
                comment.setCommentText(commentText);
                comment.setDeleteFlag(deleteFlag);
                comment.setUserId(userId);
                comment.setUserName(userName);
                comment.setCreatedDate(createdDate);
                comment.setUpdatedDate(updatedDate);

                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    //コメント登録
    public void insert(Connection connection, Comment comment) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //SQL
            sql.append("INSERT INTO comments ( ");
            sql.append("post_id");
            sql.append(", comment_text");
            sql.append(", user_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // post_id
            sql.append(", ?"); // comment_text
            sql.append(", ?"); // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            //取得した値に置き換え
            ps.setInt(1, comment.getPostId());
            ps.setString(2, comment.getCommentText());
            ps.setInt(3, comment.getUserId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

    //コメントの削除
    public void commentDelete(Connection connection, int commentId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //SQL
            sql.append("UPDATE comments");
            sql.append(" SET");
            sql.append(" delete_flag = 1,");
            sql.append(" updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" comment_id = ?");

            ps = connection.prepareStatement(sql.toString());

            //取得した値に置き換え
            ps.setInt(1, commentId);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
}
