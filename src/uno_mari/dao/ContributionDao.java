package uno_mari.dao;

import static uno_mari.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import uno_mari.beans.Post;
import uno_mari.exception.SQLRuntimeException;

public class ContributionDao {

    //新規投稿
    public void insert(Connection connection, Post post) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //SQL
            sql.append("INSERT INTO posts ( ");
            sql.append("subject");
            sql.append(", post_text");
            sql.append(", category");
            sql.append(", user_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // subject
            sql.append(", ?"); // post_text
            sql.append(", ?"); // category
            sql.append(", ?"); // user_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            //取得した値に置き換え
            ps.setString(1, post.getSubject());
            ps.setString(2, post.getPostText());
            ps.setString(3, post.getCategory());
            ps.setInt(4, post.getUserId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

}
