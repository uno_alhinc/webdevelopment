package uno_mari.dao;

import static uno_mari.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import uno_mari.beans.User;
import uno_mari.exception.NoRowsUpdatedRuntimeException;
import uno_mari.exception.SQLRuntimeException;

public class UserDao {

    public User getUser(Connection connection, String loginId,
            String password) {

        PreparedStatement ps = null;
        try {
            //login_idとpasswordが一致し、use_flag:1(利用可)のユーザーを取得
            String sql = "SELECT * FROM users WHERE login_id = ? AND password = ? AND use_flag = '1'";

            ps = connection.prepareStatement(sql);

            //取得した値に置き換え
            ps.setString(1, loginId);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();

            //ユーザーリスト取得
            List<User> userList = toUserList(rs);

            //該当ユーザーなし
            if (userList.isEmpty() == true) {
                return null;
            }
            //同一ユーザーあり
            else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                //ログインフラグ更新
                User user = userList.get(0);
                loginFlagUpdate(connection, user, 1);
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ログインIDの重複チェック
    public boolean userCheck(Connection connection, String loginId, int userId) {

        PreparedStatement ps = null;
        try {
            //ログインIDが一致する自分以外のユーザーを検索する
            String sql = "SELECT * FROM users WHERE login_id = ? and user_id <> ?";

            ps = connection.prepareStatement(sql);

            //取得した値に置き換え
            ps.setString(1, loginId);
            ps.setInt(2, userId);

            ResultSet rs = ps.executeQuery();

            //ユーザーリスト取得
            List<User> userList = toUserList(rs);

            //該当ユーザーなし
            if (userList.isEmpty() == true) {
                return true;
            }
            //同一ユーザーあり
            else {
                return false;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public boolean userCheck(Connection connection, String loginId) {

        PreparedStatement ps = null;
        try {
            //ログインIDが一致する自分以外のユーザーを検索する
            String sql = "SELECT * FROM users WHERE login_id = ? ";

            ps = connection.prepareStatement(sql);

            //取得した値に置き換え
            ps.setString(1, loginId);

            ResultSet rs = ps.executeQuery();

            //ユーザーリスト取得
            List<User> userList = toUserList(rs);

            //該当ユーザーなし
            if (userList.isEmpty() == true) {
                return true;
            }
            //同一ユーザーあり
            else {
                return false;
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ユーザーリスト作成
    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            //DBから取得したデータをリストに代入する
            while (rs.next()) {
                int userId = rs.getInt("user_id");
                String loginId = rs.getString("login_id");
                String password = rs.getString("password");
                String userName = rs.getString("user_name");
                int branchId = rs.getInt("branch_id");
                int positionId = rs.getInt("position_id");
                int loginFlag = rs.getInt("login_flag");
                int useFlag = rs.getInt("use_flag");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setUserId(userId);
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setUserName(userName);
                user.setBranchId(branchId);
                user.setPositionId(positionId);
                user.setLoginFlag(loginFlag);
                user.setUseFlag(useFlag);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    //ログインフラグ更新
    public void loginFlagUpdate(Connection connection, User user, int loginFlag) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //SQL
            sql.append("UPDATE users SET");
            sql.append("  login_flag = ?");
            sql.append(" WHERE");
            sql.append(" user_id = ?");

            ps = connection.prepareStatement(sql.toString());
            //取得した値で
            ps.setInt(1, loginFlag);
            ps.setInt(2, user.getUserId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
}