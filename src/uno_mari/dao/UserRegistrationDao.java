package uno_mari.dao;

import static uno_mari.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import uno_mari.beans.User;
import uno_mari.exception.SQLRuntimeException;

public class UserRegistrationDao {

    //新規登録
    public void insert(Connection connection, User user) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //SQL
            sql.append("INSERT INTO users ( ");
            sql.append("login_id");
            sql.append(", password");
            sql.append(", user_name");
            sql.append(", branch_id");
            sql.append(", position_id");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?"); // login_id
            sql.append(", ?"); // user_name
            sql.append(", ?"); // password
            sql.append(", ?"); // branch_id
            sql.append(", ?"); // position_id
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());

            //取得した値に置き換え
            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getUserName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getPositionId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

    //支店一覧取得
    public HashMap<Integer, String> getBranches(Connection connection) {

        PreparedStatement ps = null;
        try {

            //SELECT文
            StringBuilder sql = new StringBuilder();

            sql.append("SELECT ");
            sql.append("branch_id, ");
            sql.append("branch_name ");
            sql.append("FROM ");
            sql.append("m_branches");
            sql.append(";");

            ps = connection.prepareStatement(sql.toString());
            ResultSet rs = ps.executeQuery();
            HashMap<Integer, String> branchMap = toBranchMap(rs);

            return branchMap;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //支店マップ作成
    private HashMap<Integer, String> toBranchMap(ResultSet rs) throws SQLException {

        HashMap<Integer, String> branchMap = new HashMap<Integer, String>();
        try {
            //DBから取得したデータをマップに代入する
            while (rs.next()) {
                int branchId = rs.getInt("branch_id");
                String branchName = rs.getString("branch_name");
                branchMap.put(branchId, branchName);
            }
            return branchMap;
        } finally {
            close(rs);
        }
    }

    //部署・役職一覧取得
    public HashMap<Integer, String> getPositions(Connection connection) {

        PreparedStatement ps = null;
        try {

            //SELECT文
            StringBuilder sql = new StringBuilder();

            sql.append("SELECT ");
            sql.append("position_id, ");
            sql.append("position_name ");
            sql.append("FROM ");
            sql.append("m_positions");
            sql.append(";");

            ps = connection.prepareStatement(sql.toString());
            ResultSet rs = ps.executeQuery();
            HashMap<Integer, String> positionMap = toPositionMap(rs);

            return positionMap;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //部署・役職マップ作成
    private HashMap<Integer, String> toPositionMap(ResultSet rs) throws SQLException {

        HashMap<Integer, String> positionMap = new HashMap<Integer, String>();
        try {
            //DBから取得したデータをマップに代入する
            while (rs.next()) {
                int positionId = rs.getInt("position_id");
                String positionName = rs.getString("position_name");
                positionMap.put(positionId, positionName);
            }
            return positionMap;
        } finally {
            close(rs);
        }
    }
}
