package uno_mari.dao;

import static uno_mari.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import uno_mari.beans.User;
import uno_mari.exception.SQLRuntimeException;

public class UserManagementDao {

    public List<User> getUserList(Connection connection, String userName, String loginId, int branch, int position) {
        PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT");
            sql.append(" users.* ,");
            sql.append(" branch_Name,");
            sql.append(" position_name");
            sql.append(" FROM");
            sql.append(" users INNER JOIN m_branches");
            sql.append(" ON");
            sql.append(" users.branch_id = m_branches.branch_id");
            sql.append(" INNER JOIN m_positions");
            sql.append(" ON");
            sql.append(" users.position_id = m_positions.position_id");

            sql.append(" WHERE");
            sql.append(" user_name like ?");
            sql.append(" AND");
            sql.append(" login_id like ?");

            if (branch != 0) {
                sql.append(" AND");
                sql.append(" users.branch_id = ?");
            }

            if (position != 0) {
                sql.append(" AND");
                sql.append(" users.position_id = ?");
            }

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, "%" + userName + "%");
            ps.setString(2, "%" + loginId + "%");

            if (branch != 0 && position != 0) {

                ps.setInt(3, branch);
                ps.setInt(4, position);

            }else if(branch != 0){
                ps.setInt(3, branch);
            }else if(position != 0){
                ps.setInt(3, position);
            }

            ResultSet rs = ps.executeQuery();

            //ユーザーリスト取得
            List<User> userList = toUserList(rs);

            return userList;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

    //ユーザーリスト作成
    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            //DBから取得したデータをリストに代入する
            while (rs.next()) {

                int userId = rs.getInt("user_id");
                String loginId = rs.getString("login_id");
                String password = rs.getString("password");
                String userName = rs.getString("user_name");
                int branchId = rs.getInt("branch_id");
                int positionId = rs.getInt("position_id");
                int loginFlag = rs.getInt("login_flag");
                int useFlag = rs.getInt("use_flag");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");
                String branchName = rs.getString("branch_name");
                String positionName = rs.getString("position_name");

                User user = new User();
                user.setUserId(userId);
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setUserName(userName);
                user.setBranchId(branchId);
                user.setPositionId(positionId);
                user.setLoginFlag(loginFlag);
                user.setUseFlag(useFlag);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);
                user.setBranchName(branchName);
                user.setPositionName(positionName);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    public void useFlagUpdate(Connection connection, int userId, int useFlag) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //SQL
            sql.append("UPDATE users");
            sql.append(" SET");
            sql.append(" use_flag = ?,");
            sql.append(" updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" user_id = ?");

            ps = connection.prepareStatement(sql.toString());

            //取得した値に置き換え
            ps.setInt(1, useFlag);
            ps.setInt(2, userId);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

}
