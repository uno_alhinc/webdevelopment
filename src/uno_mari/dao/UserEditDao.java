package uno_mari.dao;

import static uno_mari.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import uno_mari.beans.User;
import uno_mari.exception.SQLRuntimeException;

public class UserEditDao {
    public User getUser(Connection connection, int userId) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE user_id = ?";

            ps = connection.prepareStatement(sql);

            //取得した値に置き換え
            ps.setInt(1, userId);

            ResultSet rs = ps.executeQuery();

            //ユーザーリスト取得
            List<User> userList = toUserList(rs);

            //該当ユーザーなし
            if (userList.isEmpty() == true) {
                return null;
            }
            //同一ユーザーあり
            else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //ユーザーリスト作成
    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {

            //DBから取得したデータをリストに代入する
            while (rs.next()) {
                int userId = rs.getInt("user_id");
                String loginId = rs.getString("login_id");
                String password = rs.getString("password");
                String userName = rs.getString("user_name");
                int branchId = rs.getInt("branch_id");
                int positionId = rs.getInt("position_id");
                int loginFlag = rs.getInt("login_flag");
                int useFlag = rs.getInt("use_flag");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setUserId(userId);
                user.setLoginId(loginId);
                user.setPassword(password);
                user.setUserName(userName);
                user.setBranchId(branchId);
                user.setPositionId(positionId);
                user.setLoginFlag(loginFlag);
                user.setUseFlag(useFlag);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    //ユーザー情報変更
    public void updateUserInfo(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //SQL
            sql.append("UPDATE users");
            sql.append(" SET");
            sql.append(" login_id = ?,");
            sql.append(" user_name = ?,");
            sql.append(" branch_id = ?,");
            sql.append(" position_id = ?,");
            sql.append(" updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" user_id = ?");

            ps = connection.prepareStatement(sql.toString());

            //取得した値に置き換え
            ps.setString(1, user.getLoginId());
            ps.setString(2, user.getUserName());
            ps.setInt(3, user.getBranchId());
            ps.setInt(4, user.getPositionId());
            ps.setInt(5, user.getUserId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //パスワード変更
    public void updatePassword(Connection connection, User user) {
        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //SQL
            sql.append("UPDATE users");
            sql.append(" SET");
            sql.append(" password = ?,");
            sql.append(" updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" user_id = ?");

            ps = connection.prepareStatement(sql.toString());

            //取得した値に置き換え
            ps.setString(1, user.getPassword());
            ps.setInt(2, user.getUserId());
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

}
