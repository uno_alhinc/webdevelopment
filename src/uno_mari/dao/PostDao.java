package uno_mari.dao;

import static uno_mari.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import uno_mari.beans.Comment;
import uno_mari.beans.Post;
import uno_mari.exception.SQLRuntimeException;
import uno_mari.service.HomeService;

public class PostDao {

    public List<Post> getPosts(Connection connection, String categoryWord, Timestamp startDate, Timestamp endDate) {
        PreparedStatement ps = null;
        try {

            StringBuilder sql = new StringBuilder();
            sql.append("SELECT *");
            sql.append(" FROM");
            sql.append(" posts INNER JOIN users");
            sql.append(" ON");
            sql.append(" posts.user_id = users.user_id");
            sql.append(" WHERE");

            //カテゴリー検索
            sql.append(" posts.category like ?");
            sql.append(" AND");

            //期間検索 (開始期間～終了期間)
            sql.append(" posts.created_date");
            sql.append(" between ? and");
            sql.append(" (? + INTERVAL 59 MINUTE + INTERVAL 59 SECOND + INTERVAL 23 HOUR)");
            sql.append(" AND");

            sql.append(" posts.delete_flag = '0'");
            sql.append(" ORDER BY");
            sql.append(" posts.updated_date");
            sql.append(" DESC");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, "%" + categoryWord + "%");
            ps.setTimestamp(2, startDate);
            ps.setTimestamp(3, endDate);

            ResultSet rs = ps.executeQuery();

            //投稿リスト取得
            List<Post> postList = toPostList(rs);

            return postList;

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    //投稿リスト作成
    private List<Post> toPostList(ResultSet rs) throws SQLException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd  HH:mm:ss");

        List<Post> ret = new ArrayList<Post>();
        try {
            //DBから取得したデータをリストに代入する
            while (rs.next()) {

                int postId = rs.getInt("post_id");
                String subject = rs.getString("subject");
                String postText = rs.getString("post_text");
                String category = rs.getString("category");
                int deleteFlag = rs.getInt("delete_flag");
                int userId = rs.getInt("user_id");
                String userName = rs.getString("user_name");
                String createdDate = sdf.format(rs.getTimestamp("created_date"));
                String updatedDate = sdf.format(rs.getTimestamp("updated_date"));

                //コメント一覧取得
                List<Comment> comments = new HomeService().getComments(postId);

                Post post = new Post();

                post.setPostId(postId);
                post.setSubject(subject);
                post.setPostText(postText);
                post.setCategory(category);
                post.setDeleteFlag(deleteFlag);
                post.setUserId(userId);
                post.setUserName(userName);
                post.setCreatedDate(createdDate);
                post.setUpdatedDate(updatedDate);
                post.setComments(comments);

                ret.add(post);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

    //投稿の削除
    public void postDelete(Connection connection, int postId) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            //SQL
            sql.append("UPDATE posts");
            sql.append(" SET");
            sql.append(" delete_flag = 1,");
            sql.append(" updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" post_id = ?");

            ps = connection.prepareStatement(sql.toString());

            //取得した値に置き換え
            ps.setInt(1, postId);
            ps.executeUpdate();

        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }
}
