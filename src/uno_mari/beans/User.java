package uno_mari.beans;

import java.sql.Timestamp;

public class User {

    private int userId;
    private String loginId;
    private String userName;
    private String password;
    private String password2;
    private int branchId;
    private String branchName;
    private int positionId;
    private String positionName;
    private int loginFlag;
    private int useFlag;
    private Timestamp createdDate;
    private Timestamp updatedDate;
    /**
     * userIdを取得します。
     * @return userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * userIdを設定します。
     * @param userId userId
     */
    public void setUserId(int userid) {
        this.userId = userid;
    }

    /**
     * loginIdを取得します。
     * @return loginId
     */
    public String getLoginId() {
        return loginId;
    }

    /**
     * loginIdを設定します。
     * @param loginId loginId
     */
    public void setLoginId(String loginid) {
        this.loginId = loginid;
    }

    /**
     * userNameを取得します。
     * @return userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * userNameを設定します。
     * @param userName userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * passwordを取得します。
     * @return password
     */
    public String getPassword() {
        return password;
    }

    /**
     * passwordを設定します。
     * @param password password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * password2を取得します。
     * @return password2
     */
    public String getPassword2() {
        return password2;
    }

    /**
     * password2を設定します。
     * @param password2 password2
     */
    public void setPassword2(String password2) {
        this.password2 = password2;
    }

    /**
     * branchIdを取得します。
     * @return branchId
     */
    public int getBranchId() {
        return branchId;
    }

    /**
     * branchIdを設定します。
     * @param branchId branchId
     */
    public void setBranchId(int branchId) {
        this.branchId = branchId;
    }

    /**
     * branchNameを取得します。
     * @return branchName
     */
    public String getBranchName() {
        return branchName;
    }

    /**
     * branchNameを設定します。
     * @param branchName branchName
     */
    public void setBranchName(String branchName) {
        this.branchName = branchName;
    }

    /**
     * positionIdを取得します。
     * @return positionId
     */
    public int getPositionId() {
        return positionId;
    }

    /**
     * positionIdを設定します。
     * @param positionId positionId
     */
    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    /**
     * positionNameを取得します。
     * @return positionName
     */
    public String getPositionName() {
        return positionName;
    }

    /**
     * positionNameを設定します。
     * @param positionName positionName
     */
    public void setPositionName(String positionName) {
        this.positionName = positionName;
    }

    /**
     * loginFlagを取得します。
     * @return loginFlag
     */
    public int getLoginFlag() {
        return loginFlag;
    }

    /**
     * loginFlagを設定します。
     * @param loginFlag loginFlag
     */
    public void setLoginFlag(int loginFlag) {
        this.loginFlag = loginFlag;
    }

    /**
     * useFlagを取得します。
     * @return useFlag
     */
    public int getUseFlag() {
        return useFlag;
    }

    /**
     * useFlagを設定します。
     * @param useFlag useFlag
     */
    public void setUseFlag(int useFlag) {
        this.useFlag = useFlag;
    }

    /**
     * createdDateを取得します。
     * @return createdDate
     */
    public Timestamp getCreatedDate() {
        return createdDate;
    }

    /**
     * createdDateを設定します。
     * @param createdDate createdDate
     */
    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * updatedDateを取得します。
     * @return updatedDate
     */
    public Timestamp getUpdatedDate() {
        return updatedDate;
    }

    /**
     * updatedDateを設定します。
     * @param updatedDate updatedDate
     */
    public void setUpdatedDate(Timestamp updatedDate) {
        this.updatedDate = updatedDate;
    }

}
