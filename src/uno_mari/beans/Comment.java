package uno_mari.beans;

public class Comment {
    private int commentId;
    private int postId;
    private String commentText;
    private int deleteFlag;
    private int userId;
    private String userName;
    private String createdDate;
    private String updatedDate;

    /**
     * commentIdを取得します。
     * @return commentId
     */
    public int getCommentId() {
        return commentId;
    }

    /**
     * commentIdを設定します。
     * @param commentId commentId
     */
    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    /**
     * postIdを取得します。
     * @return postId
     */
    public int getPostId() {
        return postId;
    }

    /**
     * postIdを設定します。
     * @param postId postId
     */
    public void setPostId(int postId) {
        this.postId = postId;
    }

    /**
     * commentTextを取得します。
     * @return commentText
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * commentTextを設定します。
     * @param commentText commentText
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    /**
     * deleteFlagを取得します。
     * @return deleteFlag
     */
    public int getDeleteFlag() {
        return deleteFlag;
    }

    /**
     * deleteFlagを設定します。
     * @param deleteFlag deleteFlag
     */
    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }

    /**
     * userIdを取得します。
     * @return userId
     */
    public int getUserId() {
        return userId;
    }

    /**
     * userIdを設定します。
     * @param userId userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }

    /**
     * userNameを取得します。
     * @return userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * userNameを設定します。
     * @param userName userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * createdDateを取得します。
     * @return createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     * createdDateを設定します。
     * @param createdDate createdDate
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * updatedDateを取得します。
     * @return updatedDate
     */
    public String getUpdatedDate() {
        return updatedDate;
    }

    /**
     * updatedDateを設定します。
     * @param updatedDate updatedDate
     */
    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

}
