package uno_mari.beans;

import java.util.List;

public class Post {

    private int postId;
    private String subject;
    private String postText;
    private String category;
    private int deleteFlag;
    private int userId;
    private String userName;
    private String createdDate;
    private String updatedDate;
    private List<Comment> comments;

    /**
     * postIdを取得します。
     * @return postId
     */
    public int getPostId() {
        return postId;
    }
    /**
     * postIdを設定します。
     * @param postId postId
     */
    public void setPostId(int postId) {
        this.postId = postId;
    }
    /**
     * subjectを取得します。
     * @return subject
     */
    public String getSubject() {
        return subject;
    }
    /**
     * subjectを設定します。
     * @param subject subject
     */
    public void setSubject(String subject) {
        this.subject = subject;
    }
    /**
     * postTextを取得します。
     * @return postText
     */
    public String getPostText() {
        return postText;
    }
    /**
     * postTextを設定します。
     * @param postText postText
     */
    public void setPostText(String postText) {
        this.postText = postText;
    }
    /**
     * categoryを取得します。
     * @return category
     */
    public String getCategory() {
        return category;
    }
    /**
     * categoryを設定します。
     * @param category category
     */
    public void setCategory(String category) {
        this.category = category;
    }
    /**
     * deleteFlagを取得します。
     * @return deleteFlag
     */
    public int getDeleteFlag() {
        return deleteFlag;
    }
    /**
     * deleteFlagを設定します。
     * @param deleteFlag deleteFlag
     */
    public void setDeleteFlag(int deleteFlag) {
        this.deleteFlag = deleteFlag;
    }
    /**
     * userIdを取得します。
     * @return userId
     */
    public int getUserId() {
        return userId;
    }
    /**
     * userIdを設定します。
     * @param userId userId
     */
    public void setUserId(int userId) {
        this.userId = userId;
    }
    /**
     * userNameを取得します。
     * @return userName
     */
    public String getUserName() {
        return userName;
    }
    /**
     * userNameを設定します。
     * @param userName userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
    /**
     * createdDateを取得します。
     * @return createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }
    /**
     * createdDateを設定します。
     * @param createdDate createdDate
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    /**
     * updatedDateを取得します。
     * @return updatedDate
     */
    public String getUpdatedDate() {
        return updatedDate;
    }
    /**
     * updatedDateを設定します。
     * @param updatedDate updatedDate
     */
    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }
    /**
     * commentsを取得します。
     * @return comments
     */
    public List<Comment> getComments() {
        return comments;
    }
    /**
     * commentsを設定します。
     * @param comments comments
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

}
