package uno_mari.service;

import static uno_mari.utils.CloseableUtil.*;
import static uno_mari.utils.DBUtil.*;

import java.sql.Connection;

import uno_mari.beans.Post;
import uno_mari.dao.ContributionDao;

public class ContributionService {
    public void register(Post post) {

        Connection connection = null;
        try {
            connection = getConnection();

            ContributionDao contributionDao = new ContributionDao();
            contributionDao.insert(connection, post);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
