package uno_mari.service;

import static uno_mari.utils.CloseableUtil.*;
import static uno_mari.utils.DBUtil.*;

import java.sql.Connection;
import java.sql.Timestamp;
import java.util.List;

import uno_mari.beans.Comment;
import uno_mari.beans.Post;
import uno_mari.dao.CommentDao;
import uno_mari.dao.PostDao;

//投稿リスト取得
public class HomeService {
    public List<Post> getPosts(String categoryWord, Timestamp startDate, Timestamp endDate) {
        Connection connection = null;
        try {
            connection = getConnection();

            PostDao postDao = new PostDao();
            List<Post> postList = postDao.getPosts(connection, categoryWord, startDate, endDate);

            commit(connection);

            return postList;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //投稿の削除
    public void deletePost(int postId) {
        Connection connection = null;
        try {
            connection = getConnection();

            new PostDao().postDelete(connection, postId);

            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //コメントリスト取得
    public List<Comment> getComments(int postId) {
        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            List<Comment> commentList = commentDao.getComments(connection, postId);

            commit(connection);

            return commentList;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //コメントの送信
    public void sendComment(Comment comment) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao commentDao = new CommentDao();
            commentDao.insert(connection, comment);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //コメントの削除
    public void deleteComment(int commentId) {
        Connection connection = null;
        try {
            connection = getConnection();

            new CommentDao().commentDelete(connection, commentId);

            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

}
