package uno_mari.service;

import static uno_mari.utils.CloseableUtil.*;
import static uno_mari.utils.DBUtil.*;

import java.sql.Connection;
import java.util.HashMap;

import uno_mari.dao.UserRegistrationDao;

public class UserRegistrationService {

    //支店一覧取得
    public HashMap<Integer, String> getBranches() {

        Connection connection = null;
        try {
            connection = getConnection();
            UserRegistrationDao branchDao = new UserRegistrationDao();
            HashMap<Integer, String> branchMap = branchDao.getBranches(connection);

            return branchMap;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    //部署・役職一覧取得
    public HashMap<Integer, String> getPositions() {

        Connection connection = null;
        try {
            connection = getConnection();
            UserRegistrationDao positionDao = new UserRegistrationDao();
            HashMap<Integer, String> positionMap = positionDao.getPositions(connection);

            return positionMap;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
