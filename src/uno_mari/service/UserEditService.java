package uno_mari.service;

import static uno_mari.utils.CloseableUtil.*;
import static uno_mari.utils.DBUtil.*;

import java.sql.Connection;

import uno_mari.beans.User;
import uno_mari.dao.UserEditDao;
import uno_mari.utils.CipherUtil;

public class UserEditService {

    public User getUser(int userId) {
        Connection connection = null;
        try {

            connection = getConnection();
            //ユーザー情報取得
            UserEditDao userEditDao = new UserEditDao();
            User user = userEditDao.getUser(connection, userId);

            commit(connection);

            return user;

        } catch (NumberFormatException e) {
            rollback(connection);
            throw e;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();
            UserEditDao userEditDao = new UserEditDao();

            //パスワードが入力されている場合は、パスワード変更
            if (!user.getPassword().equals("")) {
                String encPassword = CipherUtil.encrypt(user.getPassword());
                user.setPassword(encPassword);
                userEditDao.updatePassword(connection, user);
            }

            userEditDao.updateUserInfo(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
