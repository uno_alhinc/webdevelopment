package uno_mari.service;

import static uno_mari.utils.CloseableUtil.*;
import static uno_mari.utils.DBUtil.*;

import java.sql.Connection;

import uno_mari.beans.User;
import uno_mari.dao.UserDao;
import uno_mari.dao.UserRegistrationDao;
import uno_mari.utils.CipherUtil;

public class UserService {

    public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserRegistrationDao userRegistrationDao = new UserRegistrationDao();
            userRegistrationDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public boolean userCheck(String loginId, int UserId) {
        Connection connection = null;
        try {
            connection = getConnection();
            UserDao userDao = new UserDao();
            return userDao.userCheck(connection, loginId, UserId);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }

    public boolean userCheck(String loginId) {
        Connection connection = null;
        try {
            connection = getConnection();
            UserDao userDao = new UserDao();
            return userDao.userCheck(connection, loginId);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }

}