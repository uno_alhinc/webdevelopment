package uno_mari.service;

import static uno_mari.utils.CloseableUtil.*;
import static uno_mari.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import uno_mari.beans.User;
import uno_mari.dao.UserManagementDao;

public class UserManagementService {

    public List<User> getUsers(String userName, String loginId, int branch, int position) {
        Connection connection = null;
        try {
            connection = getConnection();
            //ユーザーリスト取得
            UserManagementDao userManagementDao = new UserManagementDao();
            List<User> userList = userManagementDao.getUserList(connection, userName, loginId, branch, position);

            commit(connection);

            return userList;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }

    public void useFlagUpdate(int userId, int useFlag) {

        Connection connection = null;
        try {
            connection = getConnection();
            //ユーザーフラグ更新
            UserManagementDao userManagementDao = new UserManagementDao();
            userManagementDao.useFlagUpdate(connection, userId, useFlag);

            commit(connection);

        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }

    }

}
